#!/bin/bash
# getting data from api using bearer token.
source .env

curl -k \
-H "Authorization: Bearer ${ACCESS_TOKEN}" \
-X GET $HOST/api/v2/rojabo_meters

echo

curl -k \
-H "Authorization: Bearer ${ACCESS_TOKEN}" \
-X GET $HOST/api/v2/users/me
echo


curl -k \
-H "Authorization: Bearer ${ACCESS_TOKEN}" \
-X GET $HOST/api/v2/training_plan

echo 

curl -k \
-H "Authorization: Bearer ${ACCESS_TOKEN}" \
-X GET $HOST/api/v1/training_sessions?from=2021-03-10\&to=2021-03-20

echo

curl -k \
-H "Authorization: Bearer ${ACCESS_TOKEN}" \
-X GET $HOST/api/v2/training_sessions?from=2021-03-10\&to=2021-03-20


echo

