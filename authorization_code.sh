source .env
grant_type=authorization_code

RESULT=$(curl -k -s \
-H "Authorization: Basic $BASIC_AUTH"  \
-d "grant_type=$grant_type&redirect_uri=$REDIRECT_URI&code=$AUTHORIZATION_CODE"  \
-X POST $TOKEN_URL)



if jq --help 2>/dev/null 1>/dev/null; then
  echo $RESULT
  echo ACCESS_TOKEN=$(echo $RESULT | jq .access_token) > .access_token
  echo REFRESH_TOKEN=$(echo $RESULT | jq .refresh_token) > .refresh_token
  echo SUCCESS: access token and refresh token is now updated
else
  echo "ERROR: jq is not installed"
fi
