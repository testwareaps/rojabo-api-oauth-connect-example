# Example for how to connect to the Rojabo API with oauth

## Preconditions

1. Linux command line
1. jq installed use i.e. `sudo apt-get install jq`


## Step 1 - Register oauth client on members.rojabo.com

1. Open https://members.rojabo.com/oauth_applications/new
1. An login as a member of Rojabo, create an account accodingly if you do not have an account.
1. Fill in the details, espacially pay attention to the callback url, which shall be public available
1. Click the register application
1. "Generate a new client secret"
1. Copy the Client Secret and write it into the .env file
1. Copy the Client ID and write it into the .env file
1. Write your callback url into the .env file


## Step 2 - Activate authorize.sh

1. Execute the `./authorize.sh` from your command prompt in linux and copy the URL created.

```
./authorize.sh

Results in:

https://members.rojabo.com/oauth/authorize?client_id=gginkoipqb5saqp8w1fx&redirect_uri=http://development.rojabo.com/oauth_demo/callback/c2logbook&state=

```


2. Copy the resulting URL into a browser address line and open the page.
3. Press "Allow and install" and you will be redirected to your callback url like

```
http://development.rojabo.com/oauth_demo/callback/?code=eyJhbGciOiJTSEEyNTYifQ.eyJ0Ijoia0szRGE0MXlEZkpwYVFvQWJGSEIiLCJzIjoiYWMiLCJleHAiOjE2NTAzNzIxNTZ9.MWMwNTc5MWFiY2I5NmMzMTYwYjYwNjI2NmZhYjQ1ZTVkMzgzM2VhNDE2OWI5ODQ2M2EyNmI4OTMyNjVmMDMzMQ&state=
```

4. Copy the `code` argument value - in this case 

```
eyJhbGciOiJTSEEyNTYifQ.eyJ0Ijoia0szRGE0MXlEZkpwYVFvQWJGSEIiLCJzIjoiYWMiLCJleHAiOjE2NTAzNzIxNTZ9.MWMwNTc5MWFiY2I5NmMzMTYwYjYwNjI2NmZhYjQ1ZTVkMzgzM2VhNDE2OWI5ODQ2M2EyNmI4OTMyNjVmMDMzMQ
```

5. Register the `code` into .env as AUTHORIZATION_CODE

6. Now you should have a Custom value  section in the .env file like this

```
CLIENT_ID=gginkoipqb5saqp8w1fx
CLIENT_SECRET=uBi5UJt0pLqH2Z5BH6vTVFKIUf4WGa197fKBR7JR8yf1TI5foY
AUTHORIZATION_CODE=eyJhbGciOiJTSEEyNTYifQ.eyJ0IjoiVXYyb0RhRnRpcjNZamN3WktLaWwiLCJzIjoiYWMiLCJleHAiOjE2NTAzNzA0NTZ9.ZmM0ODMwZDcyYWMwY2RlN2RiMThlNTE1OGUyNjg0MjVkOWZmNDFiZDk3YWUyMDRhYjQzMGMyZTA4ZjA2ZWJlZg
REDIRECT_URI=http://development.rojabo.com/oauth_demo/callback/c2logbook
```


## Step 3 - Exceute the ./authorization_code.sh

Executing the `./authorization_code.sh` should result a json string like

```
{"refresh_token":"eyJhbGciOiJTSEEyNTYifQ.eyJ0IjoiN2RaV1oxWG5BcGVXTXNrODEyUlIiLCJzIjoicnQiLCJleHAiOjE2NTU1NTY2MzB9.M2U2ZjQyMDRhYTAxOTNhY2MxZThhMzhiNGViODI3NzBhN2Y4NzFlYTc4MGUwMGM0OWE1N2NhMjk5MjMzZGU0Ng","scope":"base","access_token":"eyJhbGciOiJTSEEyNTYifQ.eyJ0IjoiN2RaV1oxWG5BcGVXTXNrODEyUlIiLCJzIjoiYXQiLCJleHAiOjE2NTAzNzYyMzB9.ODIwMmY0ZDFiYTBlYmY5ZTAxMzU5ZTA2MjliZDVlY2IyMWVmMmJmM2VhNzQyOTY0YjkyMmExZGY2YjBlYWYwYQ","token_type":"bearer","expires_in":3600}
```

The `.refesh_token` file and `.access_token` file is now updated accordingly, which means that you can now refresh the token using `./refresh_token.sh` and get data from the api with `./data-from-api.sh`.

## Step 4 - Get users data from the Rojabo API

Execute the `./data-from-api.sh` to the in this example `/api/v1/users/me` that results in 

```
{"user":{"gender":"M","weight":81.0,"level":3,"height":null,"birthdate":"1970-11-09","name":"Jakob \u00d8jvind Nielsen","email":"jakob.ojvind@gmail.com"}}
```

## Step 5 - Refresh the access token

Execute the `./refresh_token.sh` and you will get 

```
{"refresh_token":"eyJhbGciOiJTSEEyNTYifQ.eyJ0IjoiN2RaV1oxWG5BcGVXTXNrODEyUlIiLCJzIjoicnQiLCJleHAiOjE2NTU1NTY2MjN9.N2FlYjVjNWE1MzFkNWM2OTc3ZTExMDBiNTI1OWE0MmI2MTA3MzNjY2FlODUyMGMyODRhNGJjMGI4ZWY4NjRlNg","scope":"base","access_token":"eyJhbGciOiJTSEEyNTYifQ.eyJ0IjoiN2RaV1oxWG5BcGVXTXNrODEyUlIiLCJzIjoiYXQiLCJleHAiOjE2NTAzNzYyMjN9.ZWVkNGQ5NjJiMTM5NjFkMWZjYjY4NDM1ZmNhMmZlNjcxNTAzNDRkMzhjODY1MDRkNWRlMDFlMGU2NjIyNDFkMg","token_type":"bearer","expires_in":3600}
```